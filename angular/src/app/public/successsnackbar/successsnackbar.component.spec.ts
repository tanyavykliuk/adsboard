import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuccesssnackbarComponent } from './successsnackbar.component';

describe('SuccesssnackbarComponent', () => {
  let component: SuccesssnackbarComponent;
  let fixture: ComponentFixture<SuccesssnackbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuccesssnackbarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuccesssnackbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
